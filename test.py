import preprocessing
import apriori
import fp_growth
import time

'''
apriori支持度30%相当于fp_growth出现的次数为58次
'''

data_set = preprocessing.preprocess_data("超市数据集.xls")
t1 = time.time()
# 30%的支持度
L, supportData = apriori.apriori(data_set, 0.3)
# 50%的可信度
res1 = apriori.generate_rules(L, supportData, 0.5)
t2 = time.time()
print("apriori大约共耗时：" + str(t2-t1) + "秒")
t3 = time.time()
freqItems = fp_growth.FP_Growth(data_set, 58)
t4 = time.time()
print("fp_growth大约共耗时：" + str(t4-t3) + "秒")
